<!-- 
原创作者：海口龙华雨云谷网络科技工作室创始人谢星宇
作者QQ：917756877
作者Email：admin@yuyungu.com
根据2013年1月30日《计算机软件保护条例》新规定：第十七条
为了学习和研究软件内含的设计思想和原理，通过安装、显示、传输或者存储软件等方式使用软件的，可以不经软件著作权人许可，不向其支付报酬。
鉴于此，希望大家学习以及研究程序软件! 切勿商用,切勿违法使用!!!否则后果自行承担! 
-->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>海口龙华雨云谷网络科技工作室</title>
    <script async="" src="//busuanzi.ibruce.info/busuanzi/2.3/busuanzi.pure.mini.js"></script>
    <link href="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/5.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.bootcdn.net/ajax/libs/font-awesome/6.4.2/css/all.min.css" rel="stylesheet">
    <link href="./index.css" rel="stylesheet">
    
    <script type="text/javascript" src="//api.tongjiniao.com/c?_=635954018806571008" async></script>
<div class="web_notice" style="
position: fixed;
top: 0;
left: 0;
width: 100%;
height: 100%;
background: rgba(0,0,0,0.3);
z-index: 99999;
">
    <div style="position: fixed;top: 50%;left: 50%;width: 550px;background: #FFF;transform: translate(-50%, -50%);border-radius: 40px;padding: 50px 40px;">
        <h3 style="font-weight: bold;text-align: center;font-size: 30px;">网站通知
            <div style="
		font-size: 16px;
		margin-top: 26px;
		line-height: 30px;
		color: #999;">
                    <p>域名状态：</p>
                    <p>✔️yuyungu.com</p>
                    <p>✔️041x.cn</p>
                    <p><a href="https://beian.miit.gov.cn" target="_blank" rel="noopener noreferrer"><img src="./icp.png" alt="京ICP备2024063037号-1"> 京ICP备2024063037号-1</a></p>
                
                
                    <p><a href="https://beian.mps.gov.cn/#/query/webSearch?code=11011402054027/" target="_blank" rel="noopener noreferrer"><img src="./icpga.png" alt="京公网安备11011402054027"> 京公网安备11011402054027</a></p>
                    <p><a href="./yyzz.png" target="_blank" rel="noopener noreferrer"><img src="./wlgs.png" alt="营业执照"> 营业执照</a></p>
                    <p>官方邮箱：admin@yuyungu.com</p>
               </div>
            
            <a style="
		display: block;
		background: #98a3ff;
		color: #FFF;
		text-align: center;
		font-weight: bold;
		font-size: 19px;
		line-height: 60px;
		margin: 0 auto;
		margin-top: 45px;
		border-radius: 32px;
		width: 80%;
		" onclick="javascript:document.querySelector('.web_notice').remove()">
                我知道了</a>
    </div>
</div>
</head>

<link rel="stylesheet" type="./css" href="index.css">
<header>
    <nav class="navbar bg-body-tertiary navbar-transparent navbar-expand-lg" style="--bs-bg-opacity: .5;">
        <div class="container">
            <a class="navbar-brand" href="#">
                <img src="./logo.png" alt="Logo" id="logo" width="40" height="40" class="d-inline-block">
                <h1 id="title">雨云谷网络</h1>
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarColor03">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#"><i class="fa-solid fa-house"></i>Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://gitee.com/rain_cloud_valley" target="_blank"><i class="fa-solid fa-users"></i>官方码云</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="http://qm.qq.com/cgi-bin/qm/qr?_wv=1027&k=bi1chNucnzkKOtKwSsmAlIRFqM8r8ECv&authKey=qOC4NFJz3FooDoHrSmj6i7Vmb6O7WKtoUZYisKA4djOWmhhv7UwL2sfh4sYRd1Dv&noverify=0&group_code=478945193" target="_blank"><i class="fa-brands fa-qq"></i>官方QQ群</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://discord.gg/mFEf9Xu7C5" target="_blank"><i class="fa-brands fa-discord"></i>官方Discord</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://t.me/yuyungu" target="_blank"><i class="fa-brands fa-telegram"></i>官方TG群</a>
                    </li>


                </ul>
                <form class="d-flex" role="search" action="https://www.baidu.com/s?">
                    <input class="form-control me-2" type="search" autofocus="autofocus" placeholder="百度一下，你就知道~" aria-label="搜索" name="wd">
                    <button class="btn btn-outline-primary" type="submit">Search</button>
                </form>
            </div>
        </div>
    </nav>
</header>
<main>
    <div class="container text-center">
        <div class="card mb-3 mx-auto glass shadow-lg rounded-4" style="max-width: 60rem">
            <div class="card-body white-font">
                <h3 class="card-title" style="text-align:left"><i class="fa-solid fa-sitemap"></i>旗下站点</h3>
                
                    <div class="row row-cols-2 green-font button-group">
                    <div class="col-sm-3 col-md-3 col-lg-3 text-nowrap"><a href="https://idc.yuyungu.com" target="_blank" type="button" class="btn btn-primary rounded-pill link-button"><i class="fa-solid fa-link"></i>雨云谷IDC</a></div>
                    <div class="col-sm-3 col-md-3 col-lg-3 text-nowrap"><a href="https://tool.yuyungu.com" target="_blank" type="button" class="btn btn-primary rounded-pill link-button"><i class="fa-solid fa-link"></i>雨云谷工具</a></div>
                    <div class="col-sm-3 col-md-3 col-lg-3 text-nowrap"><a href="https://dh.yuyungu.com" target="_blank" type="button" class="btn btn-primary rounded-pill link-button"><i class="fa-solid fa-link"></i>雨云谷导航</a></div>
                    <div class="col-sm-3 col-md-3 col-lg-3 text-nowrap"><a href="https://movie.041x.cn" target="_blank" type="button" class="btn btn-primary rounded-pill link-button"><i class="fa-solid fa-link"></i>雨云谷影视</a></div>
                    <div class="col-sm-3 col-md-3 col-lg-3 text-nowrap"><a href="./music" target="_blank" type="button" class="btn btn-primary rounded-pill link-button"><i class="fa-solid fa-link"></i>雨云谷音乐</a></div>
                    <div class="col-sm-3 col-md-3 col-lg-3 text-nowrap"><a href="./game" target="_blank" type="button" class="btn btn-primary rounded-pill link-button"><i class="fa-solid fa-link"></i>雨云谷游戏</a></div>
                    <div class="col-sm-3 col-md-3 col-lg-3 text-nowrap"><a href="./baidu" target="_blank" type="button" class="btn btn-primary rounded-pill link-button"><i class="fa-solid fa-link"></i>帮你百度</a></div>
                </div>
            </div>
        </div>
    </div>
    <main>
        <div class="container text-center">
            <div class="card mb-3 mx-auto glass shadow-lg rounded-4" style="max-width: 60rem">
                <div class="card-body white-font">
                    <h3 class="card-title" style="text-align:left"><i class="fa-solid fa-sitemap"></i>成员个人站点</h3>

                    <div class="row row-cols-2 green-font button-group">
                        <div class="col-sm-3 col-md-3 col-lg-3 text-nowrap"><a href="https://ahaoya.cn" target="_blank" type="button" class="btn btn-primary rounded-pill link-button"><i class="fa-solid fa-link"></i>阿浩吖</a></div>
                        <div class="col-sm-3 col-md-3 col-lg-3 text-nowrap"><a href="https://paolu.host" target="_blank" type="button" class="btn btn-primary rounded-pill link-button"><i class="fa-solid fa-link"></i>跑路博客</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card mb-3 mx-auto glass shadow-lg rounded-4" style="max-width: 60rem">
            <div class="card-body white-font">
                <h3 class="card-title" style="text-align:left"><i class="fa-brands fa-linkedin"></i>合作伙伴</h3>

                <div class="row row-cols-2 green-font button-group">
                    <div class="col-sm-3 col-md-3 col-lg-3 text-nowrap"><a href="https://www.qichamao.com/orgcompany/searchitemdtl/5415e8e5b91fbc2e0d74abb4ceddd184.html" target="_blank" type="button" class="btn btn-primary rounded-pill link-button"><i class="fa-solid fa-link"></i>翼迅创联</a></div>
                    <div class="col-sm-3 col-md-3 col-lg-3 text-nowrap"><a href="http://www.pandacat.top" target="_blank" type="button" class="btn btn-primary rounded-pill link-button"><i class="fa-solid fa-link"></i>熊猫猫网络</a></div>
                    <div class="col-sm-3 col-md-3 col-lg-3 text-nowrap"><a href="https://pueh.cc" target="_blank" type="button" class="btn btn-primary rounded-pill link-button"><i class="fa-solid fa-link"></i>PUEH</a></div>
                </div>
            </div>
        </div>
    </main>

    <footer class="panel-footer" style="width: 100%; overflow: hidden;">
        <div class="container">
            <div class="row footer-top" style="text-align: center;color: white;">
                
                <h1>海口龙华雨云谷网络科技工作室</h1>
                <h2>致力于公益性服务<h2>
                <h2>
                <font size="1">董事会成员
                <li><a href="https://java2017.cn" target="_blank">谢星宇(创始人、董事会主席兼首席执行官)</a>
                <li><a href="https://paolu.host" target="_blank">邹佳凯(副创始人、董事会副主席兼首席开发员)</a>
                <li><a href="#" target="_blank">黄健凯(法人、董事会成员兼副席执行官)</a>
                <li><a href="https://ahaoya.cn" target="_blank">王佳浩(备案负责人、董事会成员兼开发员)</a>
                </h2>
                </font>
                
                </font>
                <font size="1">© 2024 <a href="https://www.qcc.com/firm/6550b4fdc91506224b8547a4ac363d4f.html" target="_blank">海口龙华雨云谷网络科技工作室.</a> 版权所有.<p>此官网为海口龙华雨云谷网络科技工作室创始人原创.</p>
                <p>官方邮箱：admin@yuyungu.com</p><p><a href="https://www.yuyungu.com/yyzz.png" target="_blank" rel="noopener noreferrer"><img src="./wlgs.png" alt="营业执照"> 营业执照 <a href="https://www.yuyungu.com/sitemap/sitemap.xml" target="_blank" rel="noopener noreferrer">| 网站地图</p>
                
                </font>


                
                <div class="col-sm-3 col-md-3 col-lg-3">
                    <a href="https://beian.miit.gov.cn" target="_blank" rel="noopener noreferrer"><img src="./icp.png" alt="京ICP备2024063037号-1"> 京ICP备2024063037号-1</a>
                </div>
                <div class="col-sm-3 col-md-3 col-lg-3">
                    <a href="https://beian.mps.gov.cn/#/query/webSearch?code=11011402054027/" target="_blank" rel="noopener noreferrer"><img src="./icpga.png" alt="京公网安备11011402054027"> 京公网安备11011402054027</a>
                </div>
                <div class="col-sm-3 col-md-3 col-lg-3">
                    <a href="#" style=" color: gray;"><img src="./moeicp.png" alt="萌ICP备20241404号"><a href="https://icp.gov.moe/?keyword=20240502" target="_blank">萌ICP备20240502号</a>

                </div>
<div class="col-sm-3 col-md-3 col-lg-3">
                    <a href="#" style=" color: gray;"><img src="./logo1xmx.png" alt="雨云谷"><a href="./dns" target="_blank">雨云谷DNS提供技术支持</a>
                    
            </div>
        </div>
    </footer>
    <script src="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/5.3.1/js/bootstrap.bundle.js"></script>
    <div id="xf-MusicPlayer" data-cdnName="https://player.xfyun.club/js"  data-themeColor="xf-sky"data-fadeOutAutoplay data-random="true"></div>
<script src="https://player.xfyun.club/js/xf-MusicPlayer/js/xf-MusicPlayer.min.js"></script>
<script src="https://player.xfyun.club/js/yinghua.js"></script>
    </body>

</html>